# _*_coding:utf-8_*_
# Created by #Suyghur, on 2019-01-14.
# Copyright (c) 2019 3KWan.
# Description : the toolkit of XML
try:
    import xml.etree.cElementTree as ElementTree
except ImportError:
    import xml.etree.ElementTree as ElementTree


class XmlToolKit:
    __ANDROID_NS__ = '{http://schemas.android.com/apk/res/android}'

    def __init__(self):
        pass

    @staticmethod
    def doc2XmlFile(_doc, fileName):
        pass

    @staticmethod
    def loadManifest(_manifestPath):
        try:
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            return manifest
        except Exception, e:
            print 'loadManifest fail , error msg : ' + str(e)

    # 获取AndroidManifest.xml中的package
    @staticmethod
    def getManifestPackageName(_manifestPath):
        try:
            manifest = XmlToolKit.loadManifest(_manifestPath)
            return manifest.get('package')
        except Exception, e:
            print 'getAndroidManifestPackageName fail , error msg : ' + str(e)

    # 获取AndroidManifest.xml中application的name
    @staticmethod
    def getManifestXmlApplicationName(_manifestPath):
        try:
            manifest = XmlToolKit.loadManifest(_manifestPath)
            application = manifest.find('application')
            return application.get(XmlToolKit.__ANDROID_NS__ + 'name')
        except Exception, e:
            print 'getManifestXmlApplicationName fail , error msg : ' + str(e)

    # 获取AndroidManifest.xml中application的appName
    @staticmethod
    def getManifestAppNameValue(_manifestPath):
        try:
            manifest = XmlToolKit.loadManifest(_manifestPath)
            application = manifest.find('application')
            return application.get(XmlToolKit.__ANDROID_NS__ + 'label')[8:]
        except Exception, e:
            print 'getManifestAppName fail , error msg : ' + str(e)

    # 获取AndroidManifest.xml中application的iconName
    @staticmethod
    def getManifestXmlIconNameValue(_manifestPath):
        try:
            manifest = XmlToolKit.loadManifest(_manifestPath)
            application = manifest.find('application')
            return application.get(XmlToolKit.__ANDROID_NS__ + 'icon')[10:]
        except Exception, e:
            print 'getManifestXmlIconNameValue fail , error msg : ' + str(e)

    # 更新AndroidManifest.xml重的package
    @staticmethod
    def updateManifestXmlPackageName(_manifestPath, _packageName):
        try:
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            manifest.set('package', _packageName)
            tree.write(_manifestPath)
        except Exception, e:
            print 'updateManifestXmlPackageName fail , error msg : ' + str(e)

    # 更新AndroidManifest.xml中的versionCode和versionName
    @staticmethod
    def updateManifestXmlVersionCodeAndName(_manifestPath, _versionCode, _versionName):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            if 'platformBuildVersionCode' in manifest.attrib:
                print 'delete platformBuildVersionCode'
                del manifest.attrib['platformBuildVersionCode']
            if 'platformBuildVersionName' in manifest.attrib:
                print 'delete platformBuildVersionName'
                del manifest.attrib['platformBuildVersionName']
            manifest.set(XmlToolKit.__ANDROID_NS__ + 'versionCode', _versionCode)
            manifest.set(XmlToolKit.__ANDROID_NS__ + 'versionName', _versionName)
            tree.write(_manifestPath)
        except Exception, e:
            print 'updateManifestXmlVersionCode fail , error msg : ' + str(e)

    # 更新AndroidManifest.xml中application的label
    @staticmethod
    def updateManifestXmlApplicationLabel(_manifestPath, _newLabel):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            application = manifest.find('application')
            application.set(XmlToolKit.__ANDROID_NS__ + 'label', _newLabel)
            tree.write(_manifestPath)
        except Exception, e:
            print 'updateManifestXmlApplicationLabel fail , error msg : ' + str(e)

    # 删除AndroidManifest.xml文件某个Activity
    @staticmethod
    def removeManifestXmlActivity(_manifestPath, _activityName):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            application = manifest.find('application')
            for activity in application.iter('activity'):
                if _activityName == activity.get(XmlToolKit.__ANDROID_NS__ + 'name'):
                    application.remove(activity)
            tree.write(_manifestPath)
        except Exception, e:
            print 'removeManifestXmlActivity fail , error msg : ' + str(e)

    # 删除AndroidManifest.xml文件某个service
    @staticmethod
    def removeManifestXmlService(_manifestPath, _serviceName):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            application = manifest.find('application')
            for service in application.iter('service'):
                if _serviceName == service.get(XmlToolKit.__ANDROID_NS__ + 'name'):
                    application.remove(service)
            tree.write(_manifestPath)
        except Exception, e:
            print 'removeManifestXmlService fail , error msg : ' + str(e)

    # 删除AndroidManifest.xml文件某个receiver
    @staticmethod
    def removeManifestXmlReceiver(_manifestPath, _receiverName):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            application = manifest.find('application')
            for receiver in application.iter('receiver'):
                if _receiverName == receiver.get(XmlToolKit.__ANDROID_NS__ + 'name'):
                    application.remove(receiver)
            tree.write(_manifestPath)
        except Exception, e:
            print 'removeManifestXmlReceiver fail , error msg : ' + str(e)

    # 删除AndroidManifest.xml文件某个provider
    @staticmethod
    def removeManifestXmlProvider(_manifestPath, _providerName):
        try:
            ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')
            tree = ElementTree.parse(_manifestPath)
            manifest = tree.getroot()
            application = manifest.find('application')
            for provider in application.iter('provider'):
                if _providerName == provider.get(XmlToolKit.__ANDROID_NS__ + 'name'):
                    application.remove(provider)
            tree.write(_manifestPath)
        except Exception, e:
            print 'removeManifestXmlProvider fail , error msg : ' + str(e)
