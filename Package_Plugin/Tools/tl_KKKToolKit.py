# _*_coding:utf-8_*_
# Created by #Suyghur, on 2019-01-19.
# Copyright (c) 2019 3KWan.
# Description : the toolkit of 3K_OriginPackage


class KKKToolKit:

    def __init__(self):
        pass

    @staticmethod
    def deleteAndCopy():
        pass

    @staticmethod
    def dealOriginPkg():
        pass

    @staticmethod
    def addGmPageActivity(_manifestPath):
        pass

    @staticmethod
    def addPermissionsGrantActivity(_manifestPath):
        pass

    @staticmethod
    def addWelcomeActivity(_manifestPath):
        pass

    @staticmethod
    def remove3KMetaData(_manifestPath):
        pass

    @staticmethod
    def delete3KStyle(_stylePath):
        pass

    @staticmethod
    def delete3KString(_stringPath):
        pass

    @staticmethod
    def delete3KColor(_colorPath):
        pass

    @staticmethod
    def delete3KDimens(_dimenPath):
        pass

    @staticmethod
    def delete3KDrawables(_drawablePath):
        pass

    @staticmethod
    def delete3KIds(_idsPath):
        pass

    @staticmethod
    def updateManifestXmlGameId(_manifestPath, _gameId):
        pass

    @staticmethod
    def updateManifestXmlGameName(_manifestPath, _gameName):
        pass

    @staticmethod
    def deal3KChanleId(_channelConfig):
        pass
